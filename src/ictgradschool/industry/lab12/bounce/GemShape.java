package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by ljam763 on 11/04/2017.
 */
public class GemShape extends Shape {
        private int Position1x = 0;
        private int Position2x = 0;
        private int Position3x = 0;
    private int Position4x = 0;
        private int Position5x = 0;
    private int Position6x = 0;
    private int Position1y = 0;
    private int Position2y = 0;
    private int Position3y = 0;
    private int Position4y = 0;
    private int Position5y = 0;
    private int Position6y = 0;
    private int X1 = 0;
    private int Y1 = 0;
    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {
        X1 = this.getX();
        Y1 = this.getY();
        if (this.getWidth() <= 40 && this.getHeight() <= 40) {
            Position1x = X1+(this.getWidth())/2;
            Position1y = Y1;
            Position2x = X1;
            Position2y = this.getHeight()/2+Y1;
            Position3x= X1+(this.getWidth())/2;
            Position3y = this.getHeight()+Y1;
            Position5x = this.getWidth()+X1;
            Position5y = this.getHeight()/2+Y1;
            int[] x_points = {Position1x,Position2x,Position3x,Position5x};
            int[] y_points = {Position1y,Position2y,Position3y, Position5y};
            Polygon a = new Polygon(x_points,y_points,4);
            painter.drawPolygon(a);
        }
        else if(this.getHeight() <= 40){
            Position1x = X1+20;
            Position1y = Y1;
            Position2x = X1;
            Position2y = this.getHeight()/2+Y1;
            Position3x= X1+20;
            Position3y = this.getHeight()+Y1;
            Position4x = this.getWidth()-20+X1;
            Position4y = this.getHeight()+Y1;
            Position5x = this.getWidth()+X1;
            Position5y = this.getHeight()/2+Y1;
            Position6x = this.getWidth()-20+X1;
            Position6y = Y1;
            int[] x_points = {Position1x,Position2x,Position3x,Position4x,Position5x,Position6x, Position1x};
            int[] y_points = {Position1y,Position2y,Position3y,Position4y, Position5y,Position6y,Position1y};
            Polygon a = new Polygon(x_points,y_points,7);
            painter.drawPolygon(a);
        }
        else if (this.getWidth() <= 40){
            Position1y = Y1+20;
            Position1x = X1;
            Position2y = Y1;
            Position2x = this.getWidth()/2+X1;
            Position3y= Y1+20;
            Position3x = this.getWidth()+X1;
            Position4y = this.getHeight()-20+Y1;
            Position4x = this.getWidth()+X1;
            Position5y = this.getHeight()+Y1;
            Position5x = this.getWidth()/2+X1;
            Position6y = this.getHeight()-20+Y1;
            Position6x = X1;
            int[] x_points = {Position1x,Position2x,Position3x,Position4x,Position5x,Position6x, Position1x};
            int[] y_points = {Position1y,Position2y,Position3y,Position4y, Position5y,Position6y,Position1y};
            Polygon a = new Polygon(x_points,y_points,7);
            painter.drawPolygon(a);
        }
    }

//        else if (this.getHeight()%this.getWidth()== 0){
//            Position1x = this.getX()+(this.getWidth())/2;
//            Position1y = this.getY();
//            Position2x = this.getX();
//            Position2y = this.getHeight()/2+this.getY();
//            Position3x= this.getX()+(this.getWidth())/2;
//            Position3y = this.getHeight()+this.getY();
//            Position5x = this.getWidth()+this.getX();
//            Position5y = this.getHeight()/2+this.getY();
//            int[] x_points = {Position1x,Position2x,Position3x,Position5x};
//            int[] y_points = {Position1y,Position2y,Position3y, Position5y};
//            Polygon a = new Polygon(x_points,y_points,4);
//            painter.drawPolygon(a);
//        }


}
